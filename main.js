const IOTA = require('iota.lib.js');
//const curl = require('curl.lib.js');
const zmq = require('zeromq')
const zmqSub = zmq.socket('sub')
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

//Initialize variables
//const targetNodeAddress = 'https://my-iota-node.com:14267'
//const nodeAddress = 'http://fabryprog-iota.eye.rs:14265'
const targetNodeAddress = 'https://iotanode.mrprunen.net:14267'
const zmqNodeAddress = 'tcp://my-iota-node.com:5556'
let iotaAddress = 'IOTA9SPAMMER9NETWORK9BY9ITALY9999999999999999999999999999999999999999999999999999'
const myMessage = 'My message to the Tangle'
const myTxTag = 'IOTA9ITALIA9RULEZ'

let txLeadTime = 0
let firingTimeInMs = 0
let arrivalTimeInMs = 0

// Create IOTA instance directly with provider
var iota = new IOTA({
  provider: targetNodeAddress
});


//Get Node info
getNodeInfo = () => {
  iota.api.getNodeInfo((err, res) => {
    if (err) {
      console.log('Problem with node connection...')
      console.log(err)
    } else {
      console.log('\n')
      console.log('Node Info: ' + targetNodeAddress)
      console.log('Application name: ' + res.appName)
      console.log('Application version: ' + res.appVersion)
      console.log('Neighbors: ' + res.neighbors)
      console.log('Node time: ' + new Date(res.time))
      getTransactionFired()
    }
  })
}

//Send TX
spamTx = () => {
  const message = iota.utils.toTrytes(myMessage)
  const transfers = [{
    address: iotaAddress,
    value: 0,
    message: message,
    tag: myTxTag 
  }]
  console.log()
  firingTimeInMs = new Date().getTime()
  console.log('TX has been fired @ '+ (firingTimeInMs)  + ' ( ' + new Date() + ' )' )
iota.api.sendTransfer(iotaAddress,3,14,transfers,(err, res) => {
  //console.log('\n')
  if (err){
    console.log('Problem with TX generation...')
    console.log(err)
  } else {
    //console.log(res)
    //console.log('Target node confirmed receival of TX @ ' + (new Date().getTime() /1000 ))
    //spamTx()
  }})
}

//Open ZMQ communication channel with IOTA node and receive TX data
getTxFromZmq = () => {
  zmqSub.connect(zmqNodeAddress)
  zmqSub.subscribe('tx')
  zmqSub.on('message', msg => {
      const data = msg.toString().split(' ') // Split to get topic & data
      switch (
      data[0] // Use index 0 to match topic
      ) {
          case 'tx':
            if(data[2]==iotaAddress) {
              //console.log(`New TX!`, data)
              arrivalTimeInMs = new Date().getTime()
              console.log(`ZMQ - TX arrived: ` + data[1] + ' @ ' + (arrivalTimeInMs) + ' ( ' + new Date() + ' )' )
              //console.log('ZMQ - TX timestamp: ' + data[5])
              //console.log('ZMQ - TX Attachment timestamp: ' + (data[11] /1000) )
              txLeadTime = arrivalTimeInMs - firingTimeInMs
              console.log()
              console.log('Lead time in milliseconds: ' + String(txLeadTime))
              console.log('Lead time in seconds: ' + String(txLeadTime/1000))
              getTransactionFired()
            }
          break
      }
       })
  }

//Start firing transactions
getTransactionFired = () => {
  console.log('')
  console.log('---------------------------------------------------' + '\n')
  readline.question(`Insert a specific IOTA address to fire the TX through the node: ` + targetNodeAddress + ' ', (userAddress) => {
    iotaAddress = userAddress
    spamTx()
    //readline.close()  
  })
}


//--------------------- START APPLICATION ---------------------
getTxFromZmq()
getNodeInfo()
