Procedure: 
1. Clone or download the repository on your workstation
2. In the project folder, execute the following commands:
	npm install
	npm start
3. The server will ask to insert the address which should be used to send and track the TX; customize your own address and paste it in the terminal:
	(Example: IOTA9SPAMMER9NETWORK9BY9ITALY9TEST9ONE9999999999999999999999999999999999999999999)
4. The server will fire a transaction on the node N�1 and will wait for the TX notification on the node N�2
	Node N�1: https://iotanode.mrprunen.net:14267
	Node N�2 (ZMQ channel used): tcp://my-iota-node.com:5556
5. Press CTRL+C to stop server